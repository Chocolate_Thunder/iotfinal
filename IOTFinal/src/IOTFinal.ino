/*
 * Project IOTFinal - Door Sensor/Room Monitor
 * Description: DOOR SENSOR THAT WILL DECTECT WHEN THE TARGETED DOOR HAS BEEN OPEN. AS WELL AS TRACK
 *              MOTION TO SEE IF THE PERSON IS MOVING INTO THE ROOM OR LEAVING THE ROOM.
 * Author: Guan Ming Chee, Jacob Hughes, Joshua Schmitz
 * Date: 11.17.2019
 */

int DAP = D0;
int DAR = D1;
int boardLed = D7;

void setup()
{
  pinMode(DAP, OUTPUT);
  pinMode(DAR, INPUT);
}

void loop()
{
  while (Particle.connected())
  {
    if (digitalRead(DAR) == HIGH)
    {
      Particle.publish("doorStatus", "DOOR OPEN", 60);
      delay(2000);
    }
    else
    {
      delay(400);
    }
  }
}